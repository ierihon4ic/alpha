package com.izag.alpha.service;

import com.izag.alpha.dto.AuthUserDto;
import com.izag.alpha.dto.RegisterUserDto;
import com.izag.alpha.dto.UpdateUserDto;
import com.izag.alpha.entity.UserEntity;
import com.izag.alpha.exception.LoginExistException;
import com.izag.alpha.exception.UserNotFoundException;
import com.izag.alpha.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApplication.class)
class UserServiceImplTest {
    private final String EXISTED_LOGIN = "EXISTED_LOGIN";

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @BeforeEach
    public void init() {
        when(userRepository.existsByLogin(anyString())).thenReturn(false);
        when(userRepository.existsByLogin(EXISTED_LOGIN)).thenReturn(true);

        when(userRepository.existsByLoginAndIdNot(anyString(), anyLong())).thenReturn(false);
        when(userRepository.existsByLoginAndIdNot(eq(EXISTED_LOGIN), anyLong())).thenReturn(true);
        when(userRepository.existsByLoginAndIdNot(EXISTED_LOGIN, 1L)).thenReturn(false);
    }

    @Test
    void registerExistLogin() {
        RegisterUserDto userDto = new RegisterUserDto();
        userDto.login = EXISTED_LOGIN;
        userDto.password = "1234";

        assertThrows(LoginExistException.class, () -> userService.register(userDto));
    }

    @Test
    void registerNotExistLogin() {
        RegisterUserDto userDto = new RegisterUserDto();
        userDto.login = "NOT_EXISTED_LOGIN";
        userDto.password = "123";
        UserEntity user = new UserEntity(1L, userDto.login, userDto.password, false);

        when(userRepository.save(any())).thenReturn(user);

        assertDoesNotThrow(() -> userService.register(userDto));

        ArgumentCaptor<UserEntity> argument = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository).save(argument.capture());
        assertAll(
                () -> Assertions.assertEquals(argument.getValue().getLogin(), userDto.login),
                () -> Assertions.assertEquals(argument.getValue().getPassword(), userDto.password),
                () -> Assertions.assertEquals(argument.getValue().getBlocked(), false)
        );
    }


    @Test
    void updateAtExistedLoginUser() {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.login = EXISTED_LOGIN;
        userDto.password = "1234";

        Optional<UserEntity> optionalUserEntity = Optional.of(new UserEntity(2L, "NOT_EXISTED_LOGIN", "123", false));
        when(userRepository.findById(2L)).thenReturn(optionalUserEntity);

        assertThrows(LoginExistException.class, () -> userService.updateUser(2L, userDto));
    }

    @Test
    void updateAtNotExistedLoginUser() {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.login = "NOT_EXISTED_LOGIN";
        userDto.password = "1234";

        Optional<UserEntity> optionalUserEntity = Optional.of(new UserEntity(1L, userDto.login, userDto.password, false));
        when(userRepository.findById(2L)).thenReturn(optionalUserEntity);

        assertDoesNotThrow(() -> userService.updateUser(2L, userDto));
        ArgumentCaptor<UserEntity> argument = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository).save(argument.capture());
        assertAll(
                () -> Assertions.assertEquals(argument.getValue().getLogin(), userDto.login),
                () -> Assertions.assertEquals(argument.getValue().getPassword(), userDto.password),
                () -> Assertions.assertEquals(argument.getValue().getBlocked(), false)
        );
    }

    @Test
    void updateNotFoundedUser() {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.login = "NOT_EXISTED_LOGIN";
        userDto.password = "1234";

        when(userRepository.findById(2L)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.updateUser(2L, userDto));
    }

    @Test
    void authSuccess() {
        UserEntity user = new UserEntity(1L, "test", "123", false);
        when(userRepository.findByLogin("test")).thenReturn(Optional.of(user));
        AuthUserDto userDto = new AuthUserDto();
        userDto.login = "test";
        userDto.password = "123";

        assertDoesNotThrow(() -> userService.auth(userDto));
    }

    @Test
    void authWrongPassword() {
        UserEntity user = new UserEntity(1L, "test", "123", false);
        when(userRepository.findByLogin("test")).thenReturn(Optional.of(user));
        AuthUserDto userDto = new AuthUserDto();
        userDto.login = "test";
        userDto.password = "1243";

        assertThrows(UserNotFoundException.class, () -> userService.auth(userDto));
    }

    @Test
    void authBlocked() {
        UserEntity user = new UserEntity(1L, "test", "123", true);
        when(userRepository.findByLogin("test")).thenReturn(Optional.of(user));
        AuthUserDto userDto = new AuthUserDto();
        userDto.login = "test";
        userDto.password = "123";

        assertThrows(UserNotFoundException.class, () -> userService.auth(userDto));
    }

    @Test
    void authNotFound() {
        when(userRepository.findByLogin("test")).thenReturn(Optional.empty());
        AuthUserDto userDto = new AuthUserDto();
        userDto.login = "test";
        userDto.password = "123";

        assertThrows(UserNotFoundException.class, () -> userService.auth(userDto));
    }

    @Test
    void block() {
        UserEntity user = new UserEntity(1L, "test", "123", false);
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        assertDoesNotThrow(() -> userService.block(1L));
        ArgumentCaptor<UserEntity> argument = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository).save(argument.capture());

        Assertions.assertEquals(argument.getValue().getBlocked(), true);
    }
}