package com.izag.alpha.controller;

import com.izag.alpha.dto.AuthUserDto;
import com.izag.alpha.dto.RegisterUserDto;
import com.izag.alpha.dto.UpdateUserDto;
import com.izag.alpha.dto.UserDto;
import com.izag.alpha.exception.LoginExistException;
import com.izag.alpha.exception.UserNotFoundException;
import com.izag.alpha.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterUserDto userDto) throws LoginExistException {
        return ResponseEntity.ok(userService.register(userDto));
    }

    @PutMapping("/{id}/")
    public ResponseEntity<?> update(@Valid @RequestBody UpdateUserDto userDto, @PathVariable Long id) throws UserNotFoundException, LoginExistException {
        return ResponseEntity.ok(userService.updateUser(id, userDto));
    }

    @GetMapping("/auth")
    public ResponseEntity<UserDto> auth(@Valid @RequestBody AuthUserDto userDto) throws UserNotFoundException {
        return ResponseEntity.ok(userService.auth(userDto));
    }

    @PostMapping("/{id}/block")
    public ResponseEntity<?> block(@PathVariable Long id) throws UserNotFoundException {
        userService.block(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
