package com.izag.alpha.exception;

public class LoginExistException extends Exception {

    public LoginExistException(String login) {
        super(login + " уже занят!");
    }

}
