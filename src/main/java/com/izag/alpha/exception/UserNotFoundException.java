package com.izag.alpha.exception;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(Long userId) {
        super("Пользователь с ID " + userId + " не найден!");
    }

    public UserNotFoundException(String login) {
        super("Пользователь с логином " + login + " не найден!");
    }
}
