package com.izag.alpha.dto;

import com.izag.alpha.entity.UserEntity;

public class UserDto {
    public Long id;
    public String login;

    public UserDto() {
    }

    public UserDto(UserEntity user) {
        this.id = user.getId();
        this.login = user.getLogin();
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", login='" + login + '\'' +
                '}';
    }
}
