package com.izag.alpha.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RegisterUserDto {
    @NotBlank(message = "Введите логин.")
    @Size(max = 20, message = "Максимальная длина логина - 20 символов.")
    public String login;
    @NotBlank(message = "Введите пароль.")
    @Size(max = 10, message = "Максимальная длина пароля - 10 символов.")
    public String password;
}
