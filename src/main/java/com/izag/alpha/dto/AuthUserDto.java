package com.izag.alpha.dto;

import javax.validation.constraints.NotBlank;

public class AuthUserDto {
    @NotBlank(message = "Введите логин")
    public String login;
    @NotBlank(message = "Введите пароль")
    public String password;
}
