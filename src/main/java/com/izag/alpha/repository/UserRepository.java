package com.izag.alpha.repository;

import com.izag.alpha.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByLogin(String login);
    Boolean existsByLogin(String login);
    Boolean existsByLoginAndIdNot(String login, Long id);
}
