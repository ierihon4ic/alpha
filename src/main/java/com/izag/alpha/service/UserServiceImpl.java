package com.izag.alpha.service;

import com.izag.alpha.dto.AuthUserDto;
import com.izag.alpha.dto.RegisterUserDto;
import com.izag.alpha.dto.UpdateUserDto;
import com.izag.alpha.dto.UserDto;
import com.izag.alpha.entity.UserEntity;
import com.izag.alpha.exception.LoginExistException;
import com.izag.alpha.exception.UserNotFoundException;
import com.izag.alpha.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDto register(RegisterUserDto userDto) throws LoginExistException {
        if (userRepository.existsByLogin(userDto.login)) {
            log.warn("Неудачная попытка регистрации: логин {} уже занят!", userDto.login);
            throw new LoginExistException(userDto.login);
        }

        UserEntity user = new UserEntity();
        user.setLogin(userDto.login);
        user.setPassword(userDto.password);
        user.setBlocked(false);
        UserDto response = new UserDto(userRepository.save(user));
        log.info("Зарегистрирован новый пользователь: {}", response);
        return response;
    }

    @Override
    public UserDto updateUser(Long userId, UpdateUserDto userDto) throws UserNotFoundException, LoginExistException {
        if (userRepository.existsByLoginAndIdNot(userDto.login, userId)) {
            log.warn("Неудачная обновления пользователя {}: логин {} уже занят!", userId, userDto.login);
            throw new LoginExistException(userDto.login);
        }

        UserEntity user = findUserById(userId);
        user.setLogin(userDto.login);
        user.setPassword(userDto.password);
        userRepository.save(user);

        UserDto response = new UserDto(user);
        log.info("Профиль пользователя {} был обновлён: {}", userId, userDto.login);
        return response;
    }

    @Override
    public UserDto auth(AuthUserDto userDto) throws UserNotFoundException {
        UserEntity user = findUserByLogin(userDto.login);

        if (user.getPassword().equals(userDto.password) && !user.getBlocked())
            return new UserDto(user);
        else {
            throw new UserNotFoundException(userDto.login);
        }
    }

    @Override
    public void block(Long userId) throws UserNotFoundException {
        UserEntity user = findUserById(userId);

        user.setBlocked(true);
        userRepository.save(user);
        log.info("Пользователь {} был заблокирован!", userId);
    }


    private UserEntity findUserById(Long userId) throws UserNotFoundException {
        return userRepository
                .findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    private UserEntity findUserByLogin(String login) throws UserNotFoundException {
        return userRepository
                .findByLogin(login)
                .orElseThrow(() -> new UserNotFoundException(login));
    }
}
