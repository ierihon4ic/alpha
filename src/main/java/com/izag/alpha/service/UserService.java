package com.izag.alpha.service;

import com.izag.alpha.dto.AuthUserDto;
import com.izag.alpha.dto.RegisterUserDto;
import com.izag.alpha.dto.UpdateUserDto;
import com.izag.alpha.dto.UserDto;
import com.izag.alpha.exception.LoginExistException;
import com.izag.alpha.exception.UserNotFoundException;

public interface UserService {
    UserDto register(RegisterUserDto userDto) throws LoginExistException;
    UserDto updateUser(Long userId, UpdateUserDto userDto) throws UserNotFoundException, LoginExistException;
    UserDto auth(AuthUserDto userDto) throws UserNotFoundException;

    void block(Long userId) throws UserNotFoundException;
}
