JAVA_OPT=-Xmx1024m
JARFILE=alpha-0.0.1.jar
PID_FILE=pid.file
RUNNING=N

if [ -f $PID_FILE ]; then
  PID=$(cat $PID_FILE)
  if [ ! -z "$PID" ] && kill -0 $PID 2>/dev/null; then
    RUNNING=Y
  fi
fi

start() {
  if [ $RUNNING == "Y" ]; then
    echo "Application already started"
  else
    if [ -z "$JARFILE" ]; then
      echo "ERROR: jar file not found"
    else
      nohup java $JAVA_OPT -jar $JARFILE >>nohup.out 2>&1 &
      echo $! >$PID_FILE
      echo "Application $JARFILE starting..."
      tail -f nohup.out

    fi
  fi
}

stop() {
  if [ $RUNNING == "Y" ]; then
    kill -9 $PID
    rm -f $PID_FILE
    echo "Application stopped"
  else
    echo "Application not running"
  fi
}

restart() {
  stop
  RUNNING=N
  start
}

case "$1" in

'start')
  start
  ;;

'stop')
  stop
  ;;

'restart')
  restart
  ;;

*)
  echo "Usage: $0 {  start | stop | restart  }"
  exit 1
  ;;
esac
exit 0
